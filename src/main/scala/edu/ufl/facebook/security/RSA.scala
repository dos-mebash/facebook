package edu.ufl.facebook.security


import java.io._
import java.nio.charset.Charset
import java.security.{KeyFactory, _}
import java.security.spec.{PKCS8EncodedKeySpec, X509EncodedKeySpec}
import javax.crypto._
import org.apache.commons.codec.binary.Base64


object RSA {

  private val ALGO: String = "RSA"
  private val ALG_TRANSFORMATION = "RSA/ECB/OAEPWithSHA-1AndMGF1Padding"

  //  def encrypt(encodedByteArray: Array[Byte], key: String): String = {
  //    val text = Base64.encodeBase64String(encodedByteArray)
  //    val decodedKey = Base64.decodeBase64(key)
  //    val keySpec = new X509EncodedKeySpec(decodedKey)
  //    val keyFactory = KeyFactory.getInstance(ALGO)
  //    val pubKey = keyFactory.generatePublic(keySpec)
  //    encrypt(text, pubKey)
  //  }

  private val charSet: String = "ISO-8859-1"

  def encrypt(text: String, keyString: String): String = {
    val key = decodePrivateKey(keyString)
    val cipher = Cipher.getInstance(ALG_TRANSFORMATION)
    cipher.init(Cipher.ENCRYPT_MODE, key)
    val cipherText = cipher.doFinal(text.getBytes(Charset.forName(charSet)))
    Base64.encodeBase64String(cipherText)
  }

  def encrypt1(text: String, keyString: String): String = {
    val key = decodePublicKey(keyString)
    val cipher = Cipher.getInstance(ALG_TRANSFORMATION)
    cipher.init(Cipher.ENCRYPT_MODE, key)
    val cipherText = cipher.doFinal(text.getBytes(Charset.forName(charSet)))
    Base64.encodeBase64String(cipherText)
  }

  def decodePublicKey(encodedKey: String): PublicKey = {
    val publicBytes = Base64.decodeBase64(encodedKey)
    val keySpec = new X509EncodedKeySpec(publicBytes)
    val keyFactory = KeyFactory.getInstance(ALGO)
    keyFactory.generatePublic(keySpec)
  }

  def encrypt(text: String, key: PublicKey): String = {
    val cipher = Cipher.getInstance(ALG_TRANSFORMATION)
    cipher.init(Cipher.ENCRYPT_MODE, key)
    val cipherText = cipher.doFinal(text.getBytes(Charset.forName(charSet)))
    Base64.encodeBase64String(cipherText)
  }


  def decrypt(text: String, key: PrivateKey): String = {
    val encryptedByteArray = Base64.decodeBase64(text)
    val cipher = Cipher.getInstance(ALG_TRANSFORMATION)
    cipher.init(Cipher.DECRYPT_MODE, key)
    val decryptedText = cipher.doFinal(encryptedByteArray)
    Base64.encodeBase64String(decryptedText)
  }


  def generateKeyPair: KeyPair = {
    val random = SecureRandom.getInstance("SHA1PRNG")
    val keyGen = KeyPairGenerator.getInstance("RSA")
    keyGen.initialize(1024, random)
    keyGen.genKeyPair()
  }

  def encodePublicKey(key: PublicKey): String = {
    Base64.encodeBase64String(key.getEncoded())
  }

  def decodePrivateKey(encodedKey: String): PrivateKey = {
    val privateBytes = Base64.decodeBase64(encodedKey)
    val keySpec = new PKCS8EncodedKeySpec(privateBytes)
    val keyFactory = KeyFactory.getInstance(ALGO)
    keyFactory.generatePrivate(keySpec)
  }

  def decrypt(text: String, key: String): String = {
    val privateKey = decodePrivateKey(key)
    decrypt(text, privateKey)
  }

  def loadKeyPair(path: String, algorithm: String): KeyPair = {
    // Read Public Key.
    val filePublicKey: File = new File(path + "/public.key")
    var fis: FileInputStream = new FileInputStream(path + "/public.key")
    val encodedPublicKey: Array[Byte] = new Array[Byte](filePublicKey.length().toInt)
    fis.read(encodedPublicKey)
    fis.close()

    // Read Private Key.
    val filePrivateKey: File = new File(path + "/private.key")
    fis = new FileInputStream(path + "/private.key")
    val encodedPrivateKey: Array[Byte] = new Array[Byte](filePrivateKey.length().toInt)
    fis.read(encodedPrivateKey)
    fis.close()

    // Generate KeyPair.
    val keyFactory: KeyFactory = KeyFactory.getInstance(algorithm)
    val publicKeySpec: X509EncodedKeySpec = new X509EncodedKeySpec(encodedPublicKey)
    val publicKey: PublicKey = keyFactory.generatePublic(publicKeySpec)

    val privateKeySpec: PKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey)
    val privateKey: PrivateKey = keyFactory.generatePrivate(privateKeySpec);

    new KeyPair(publicKey, privateKey);
  }

  def checkModulus(publicK: String, privateK: String) = {
    val pub = Base64.decodeBase64(publicK)
    val priv = Base64.decodeBase64(privateK)

    val keySpec = new PKCS8EncodedKeySpec(priv)
    val keyFactory = KeyFactory.getInstance(ALGO)
    val privateKey: PrivateKey = keyFactory.generatePrivate(keySpec)

    val keySpec1 = new X509EncodedKeySpec(pub)
    //    val keyFactory = KeyFactory.getInstance(ALGO)
    val publicKey: PublicKey = keyFactory.generatePublic(keySpec1)

    checkData(privateKey, publicKey)

  }

  def checkData(priv: PrivateKey, pub: PublicKey):String= {
    var data = Base64.encodeBase64String("Hello".getBytes)
    var enc = encrypt(data, pub)
    var key = "QorU7uYnx7dInIYZe+WbFFPGC6+8iZ3/OLT7M367e0TWDGfrxWcp0Upf+IJkX828vOzY6c7ZTdmW+40wLgoNkBF/9k2xfCv5dnTKMErKlvjuCupR5xrS2WhS75y/wJAp61gbRVei9X09tZFD0v7FcR7pajDU55twhMhc5mk+T6g="
    var dec = decrypt(key, priv)
    println(new String(Base64.decodeBase64(Base64.decodeBase64(dec))))
    dec = new String(Base64.decodeBase64(Base64.decodeBase64(dec)))
    dec
  }

  def main(args: Array[String]) {

    var publicK = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCPWVLYS2BMkE07sLemX2YMSDY+1Ry2VGudMuIyTusjZD2bzx8fzQm5OpSejQ0jEYcvfnYIeAg061xFUlCZTmjl/Ef/erU/RkY51nurgdp8oyPyHQ3uwcKbIJRlzazjjKevRWpH3ABFcucRziFfzyifi9IRnC63COMqJFvyYBUd/QIDAQAB"
    var privateK = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAL/n+neNCVKsOMxkL9VUpCWKOm0CRtMHKfvGKn1V+zz9bHuBFu34eP1V+xVnjLzXZDYi0ACLG8wJ3UjdseEfyP5IW7dVCwHfDS+sTvgEbFkj5O/jmKGO1nKQo7VBA7GxjLOytO6OMGYyrNiBeTTUd9J5YSyZzJswFiyx0diB/QYxAgMBAAECgYEAgSMUq8BPhr931SqR+adkchKLcHF4wWbvvIpEa2UhlJAgoRdkG6z5ldh3BpdQ4H6846UXfVYe1XXJ38+3dHuUH0RUXX4VdxsvPoTApFSaAZQiHql1pJYV6nCfdNO2EVLv5GuJ/J0GqrDG56ihaINrKck01S9qA/PA24k0RAhOXPUCQQD7kUPoqlt/o6G36bIC4Pt3ex+q8WdQTmeongtV4BiLFt8pAqS6VYU89PKlG+p9EnOTiAG+Pxcf74aL9IlEc+HvAkEAw0mZ+38c2JetAruP6vkhABSa5P9nLOknoaaDgzoXhCHKjowEHfDVmN88ISVRsnhi3z0hbdn4MkdhhEgZoRM53wJAL7WcCGsOkUVqLSPv86XRaxZ5qgTAUDWeaRcYFUA7x10eE3wB26jObQITZobF0NSIAMnBcuAqNY9KmA5XXhkQFwJAJ0knMPDhXLEmnOYejaaQfPOoNWkr2F0PG/XjfwKwuGQT25lPOra4QYinEAPx63VMbpxdnTu8MmEpvPLENSMpAQJBANkUJ/oModFy68IcMlyxKiu9CTk4/T9n3GNSFt86gY4V26Gki+/iSHqZpscwtrYTDtkTJd3kEXgrcAb0uIGNcVQ="

    var key = "mkyF41bCWKlwJiPGqVTbWheGUdWapW2yasJTzO6jsUIM9FctRR07Srm0Zyb0MF2Ze2UI3rMjSFbyNulJwzAxrDnkEHZuS6LPy7YtdygslggMUz8Kg+4QFdwtSBmmTHsdvAkXhrEKpyBrnVQ0LaBfj3V99hZkXJOvU/WobNu/Emg="
    val AESKeyOfTheCircle = new String(Base64.decodeBase64(decrypt(key, privateK)))
    println("The AES Key is " + AESKeyOfTheCircle)

    //var aes = checkModulus(publicK, privateK)

    var key1 = "u3eOZTD/Y4Y5/eMBNo2EQGZp5dBHSYeu+lzgN0Tw5D73gqLGHUvTZrQzR10T6gKuja0L9BKzgaHHK7AdDmpTmP5JgDnKY8CP48xycOB87ulWAx6MqBWpk2n0sqwapIheRMxu7U8Wbuaro5CegWJUf2lnUyMQ2Gv6noCqpgeitdg="
    var priv = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMun653ZG5nOyQ+yNttNfXHxd2SiBUoMVKymiOy/QPv6DOQfjWbgMoV462s87HvHMitMBiS+7c1LKDvfijMt00sc/ayxFGV6Bo15Uuo+kuGOaO8ZCGFyP5DfTP7Nr4ZZOrUaGbJtooIa3/2ljAKIjxZcQqaSOqT5v+Pa9xIRqiIpAgMBAAECgYBhkbOaTjeg+qbtNC5/5qIhAtR2jAEIttuiU67p7b8OeX/PkyZFkUfjFkmDeQ67RnXMtIGE/UwkvMsr1C770gqV6RrMDbw0NoquI9LKyQ4TTjvjotn3ZGOp18fbxvlvj2l5QGYX4+JsMpiXvo0jQyFmP/JI+Akonu5GcvqqR4TcmQJBAOkIvVrrPaF4J7ymGdFtyM/y9t9iqTOpF4g91MmpT7xMo9co61XcXQH1XULwDzXyc9nmfl4H7dvT0yGQ3iD9mcsCQQDfuf4ns19MloM7WtGaB8pa1UBLigM0LOsZw7dfUOTcOXK+kUhsC3sKwXtTrmlIoHB4FRWtQi9g61pWzKXoGoVbAkB2UIOjN7CQ3KqAM20EUdMVEWyaOkNiAUX7XDk/MgFTvy2oPhROAZYPOn5hT/TJFRIQ3d/m6euwIrodXqoI4flFAkEA3Nvmu9iGlrvrN1gTnQRdzkfBHhFT0lkZiii8yJq6I3GPBfO0U/0NbXPNxLFj/C9ueA0QhibVUM9mMlP64GP2CwJAQ/KSZ6OHXWisLTDz23j7Ckk3BGT/OPgsklv4h49r+wpfjY9b8yiLzKIMrX78MOJ+07KnidKUpxL3pydIpYSyVw=="
    var enc1 = new String(Base64.decodeBase64(decrypt(key1, priv)))
    println(enc1)

    //    println("Now with self generated")

    //    for(i<-0 to 10) {
    //      val keyPair = generateKeyPair
    //      val privateKey = keyPair.getPrivate
    //      val publicKey = keyPair.getPublic
    //      val an = Base64.encodeBase64String(publicKey.getEncoded)
    //      val ans = Base64.encodeBase64String(privateKey.getEncoded)
    //      println(an)
    //      println(ans)
    //      println()
    //      println()
    //    }

    //    checkData(privateKey, publicKey)

    //    val filePub = new ObjectOutputStream(new FileOutputStream("/tmp/id_rsa.pub"))
    //    filePub.writeObject(publicKey)
    //    filePub.close()
    //
    //    val filePrivate = new ObjectOutputStream(new FileOutputStream("id_rsa"))
    //    filePrivate.writeObject(privateKey)
    //    filePrivate.close()
    //
    //    val inputStream = new ObjectInputStream(new FileInputStream("/tmp/id_rsa.pub"))
    //    val serverPublicKey = inputStream.readObject().asInstanceOf[PublicKey]
    //
    //    val string = "mebin"
    //    val text = encrypt(string, serverPublicKey)
    //    println(text)
    //    println(new String(Base64.decodeBase64(decrypt(text, privateKey))))
  }
}
