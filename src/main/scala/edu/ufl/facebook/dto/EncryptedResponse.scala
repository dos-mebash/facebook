package edu.ufl.facebook.dto

import edu.ufl.facebook.entity.{EncryptedData, EncryptedDataJsonImplicits, Service}
import spray.json.DefaultJsonProtocol

/**
 * Created by mebin on 12/15/15 for Facebook
 */
case class EncryptedResponse(encryptedData: EncryptedData, aesKey: String)

object EncryptedResponseJsonImplicits extends DefaultJsonProtocol {

  import EncryptedDataJsonImplicits._

  implicit val encDataResponse = jsonFormat2(EncryptedResponse)
}

object EncryptedResponseService extends Service[EncryptedResponse] {
  def toJson(e:EncryptedResponse): String={
    toJson(e)
  }
}