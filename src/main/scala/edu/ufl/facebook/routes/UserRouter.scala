package edu.ufl.facebook.routes

import edu.ufl.facebook.Utility
import edu.ufl.facebook.dto.{EncryptedResponseJsonImplicits, EncryptedResponse, EncryptedResponseService}
import edu.ufl.facebook.entity._
import spray.httpx.SprayJsonSupport.sprayJsonUnmarshaller

/**
  * Created by mebin on 11/18/15 for Facebook
  */
class UserRouter extends Router {

  def receive = runRoute(userRoute)

  val userRoute = {
    import EncryptedDataJsonImplicits._
    import UserJsonImplicits._
    //import spray.httpx.SprayJsonSupport.sprayJsonUnmarshalle
    getJson {
      path("all") {
        complete(UserService.futureToJSONWait(UserRepository.findAll))
      } ~
        path("allIds") {
          //println("the req was received")
          complete(UserService.futureToJSONWait(UserRepository.findAllIds))
        } ~
        path(Segment) {
          id =>
            val f = UserRepository.findById(id)
            complete(UserService.futureToJSONWait(f))
        } ~
        path(Segment / "friends_all") { userId =>
          complete(UserService.toJSON(UserService.getFriendList(userId)))
        } ~
        path("publicKey" / Segment) {
          id =>
            val user = UserService.getUser(id)
            complete(user.key)
        } ~
        path("getFriendRequest" / Segment) {
          id =>
            println(UserService.getFriendRequests(id))
            complete(UserService.getFriendRequests(id))
        }
    } ~
      post {
        path("register") {
          secure {
            entity(as[User]) { person =>
              val p = person.copy(id = Utility.uuid)
              val f = UserRepository.save(p)
              ProfileRepository.save(new Profile(p.id, p.name, null, null))
              UserService.futureToJSONWait(f)
              clientKeys += p.id -> p.key
              complete(p.id)
            }
          }
        }
      } ~
      path(Segment / "updateName") { userId =>
        post {
          import UserJsonImplicits._
          entity(as[Map[String, String]]) { m: Map[String, String] => complete {
            if (m.contains("name")) {
              val fName: String = m.get("name") match {
                case Some(fName) => fName
              }
              if (UserService.updateName(userId, fName) == true) {
                "\"success\""
              }
              else {
                "failed"
              }
            }
            else {
              "failed"
            }
          }
          }
        } ~
          delete {
            complete(UserService.futureToJSONWait(UserRepository.delete(userId)))
          }
      } ~
      path("addPost" / Segment) {
        userId => {
          post {
            entity(as[EncryptedData]) { enc =>
              val fromUser = UserService.getUser(userId)
              val listOfPost = fromUser.posts match {
                case Some(p) => {
                  val newP = enc :: p
                  Option(newP)
                }
                case None => Option(List(enc))
              }
              val user = fromUser.copy(posts = listOfPost)
              UserRepository.update(user)
              complete("Request Submitted")
            }
          }
        }
      } ~
      path("addPic" / Segment) {
        userId => {
          post {
            entity(as[EncryptedData]) { enc =>
              val fromUser = UserService.getUser(userId)
              val listOfPhotos = fromUser.photos match {
                case Some(p) => {
                  val newP = enc :: p
                  Option(newP)
                }
                case None => Option(List(enc))
              }
              val user = fromUser.copy(photos = listOfPhotos)
              UserRepository.update(user)
              complete("Request Submitted")
            }
          }
        }
      } ~
      path(Segment / "acceptFriendRequest" / Segment) { (userId, friendId) =>
        post {
          entity(as[String]) { encKeyOfFriend =>
            //encKey is aes key encrypted with public key of friend {
            UserService.addFriend(userId, friendId, encKeyOfFriend)
            UserService.friendRequestAccepted(friendId, userId)
            complete("success")
          }
        }
      } ~
      path(Segment / "SendFriendRequest" / Segment) {
        (userId, friendId) => {
          post {
            entity(as[String]) { friendEncId =>
              if (UserService.updateFriendRequests(userId, friendId, friendEncId)) {
                complete("Success")
              }
              else {
                complete("Failure")
              }
            }
          }
        }
      } ~
      put {
        path(Segment / "likePage" / Segment) {
          (userId, pageId) =>
            PageService.setLikes(pageId, userId)
            complete("success")
        }
      } ~
      getJson {
        path(Segment / "post" / Segment) { (userId, postId) =>
          headerValueByName("userId") { loginUserId =>
            val user = UserService.getUser(userId)
            val posts = user.posts match {
              case Some(postList) => postList
              case None => List()
            }
            val matchedPostList = posts.filter(_.entityId == postId)
            val response = matchedPostList(0)
            val friendCircle = Circle.idAndListOfCircles.get(userId).get(0)
            val key = friendCircle.idAndEncKeyMap(loginUserId)
            val encResponse = EncryptedResponse(response, key)
            complete(EncryptedResponseService.toJson(encResponse))
          }
        } ~
          path(Segment / "randomPost") { (userId) => {
            headerValueByName("userId") { loginUserId =>
              println("inside random post")
              val user = UserService.getUser(userId)
              val posts = user.posts match {
                case Some(postList) => postList
                case None => List()
              }
              if (!posts.isEmpty) {
                val matchedPostList = posts(0)
                val response = matchedPostList
                val friendCircle = Circle.idAndListOfCircles.get(userId).get(0)
                val key = friendCircle.idAndEncKeyMap.getOrElse(loginUserId, "-1")
                if (!key.equals("-1")) {
                  val encResponse = EncryptedResponse(response, key)
                  import spray.json._
                  import EncryptedResponseJsonImplicits._
                  println(encResponse.toJson)
                  val responseJson = encResponse.toJson.toString
                  complete(responseJson)
                }
                else {
                  complete("Un-Authorized Access!!")
                }
              }
              else {
                complete(s"No Posts available for ${userId}")
              }
            }
          }
          } ~
          path(Segment / "randomPic") { (userId) => {
            headerValueByName("userId") { loginUserId =>
              println("inside random pic")
              val user = UserService.getUser(userId)
              val photos = user.photos match {
                case Some(photoList) => photoList
                case None => List()
              }
              if (!photos.isEmpty) {
                val matchedPhotoList = photos(0)
                val response = matchedPhotoList
                val friendCircle = Circle.idAndListOfCircles.get(userId).get(0)
                val key = friendCircle.idAndEncKeyMap.getOrElse(loginUserId, "-1")
                if (!key.equals("-1")) {
                  val encResponse = EncryptedResponse(response, key)
                  import spray.json._
                  import EncryptedResponseJsonImplicits._
                  println(encResponse.toJson)
                  val responseJson = encResponse.toJson.toString
                  complete(responseJson)
                }
                else {
                  println("unauthorized response")
                  complete("Un-Authorized Access!!")
                }
              }
              else {
                println(" No posts avail")
                complete(s" No Posts available for ${userId}")
              }
            }
          }
          }
      }
  }
}
