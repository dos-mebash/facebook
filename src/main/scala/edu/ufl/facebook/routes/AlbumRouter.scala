package edu.ufl.facebook.routes

import edu.ufl.facebook.Utility
import edu.ufl.facebook.entity._
import edu.ufl.facebook.entity.AlbumJsonImplicits._
import spray.httpx.SprayJsonSupport.sprayJsonUnmarshaller
import scala.concurrent.duration._
import scala.concurrent.Await

/**
  * Created by mebin on 11/20/15 for Facebook
  */
class AlbumRouter extends Router {

  def receive = runRoute(albumRoute)

  val albumRoute = {

    getJson {
      path(Segment) {
        id =>
          val f = AlbumRepository.findById(id)
          complete(AlbumService.futureToJSONWait(f))
      }
    } ~
      post {
        path("save") {
          entity(as[Album]) { album =>
            val alb = album.copy(id = Utility.uuid)
            val f = AlbumRepository.save(alb)
            complete(AlbumService.futureToJSONWait(f))
          }
        } ~
          path(Segment / "addPhotos" / Segment) {
            (albumId, photoId) => {
              val pic = PictureRepository.findById(photoId)
              Await.result(pic, 2 seconds) match {
                case Some(p: Picture) =>
                  complete(AlbumService.updatePhotos(albumId, p).toString())
                case None => complete("Failure")
              }
            }
              complete("Failure")
          }
        // no deletion and update supported again
      }
  }
}
