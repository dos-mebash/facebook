package edu.ufl.facebook.routes

import edu.ufl.facebook.Utility
import edu.ufl.facebook.entity._
import scala.concurrent.duration._
import scala.concurrent.Await

/**
  * Created by mebin on 11/20/15 for Facebook
  */
class PageRouter extends Router {
  def receive = runRoute(pageRoute)

  val pageRoute = {
    import edu.ufl.facebook.entity.PageJsonImplicits._
    import spray.httpx.SprayJsonSupport.sprayJsonUnmarshaller
    getJson {
      path("all") {
        complete(PageService.futureToJSONWait(PageRepository.findAll))
      } ~
        path("allIds") {
          complete(PageService.futureToJSONWait(PageRepository.findAllIds))
        } ~
        path(Segment) {
          id =>
            val f = PageRepository.findById(id)
            complete(PageService.futureToJSONWait(f))
        }
    } ~
      post {
        path("register") {
          entity(as[Page]) { page =>
            val p = page.copy(id = Utility.uuid)
            val f = PageRepository.save(p)
            ProfileRepository.save(new Profile(p.id, p.name, null, null))
            //clientKeys += p.id -> p.key
            PageService.futureToJSONWait(f)
            complete{p.id}
          }
        } ~
          path("save") {
            entity(as[Page]) { page: Page =>
              val pag = page.copy(id = Utility.uuid)
              val f = PageRepository.save(pag)
              complete(PageService.futureToJSONWait(f))
            }
          } ~
          path(Segment / "profilePic" / Segment) {
            (pageId, picId) =>
              val page = PageService.getPage(pageId)
              val pic = PictureService.getPicture(picId)
              val p = page.copy(profilePic = Option(pic))
              PageRepository.update(p)
              complete("success")
          } ~
          path(Segment / "addphoto" / Segment) {
            (pageId, picId) =>
              val pic = PictureService.getPicture(picId)
              complete(PageService.updatePhotos(pageId, pic).toString())
          } ~
          path(Segment / "addFeed" / Segment) {
            (pageId, postId) => {
              val post = PostRepository.findById(postId)
              val result = Await.result(post, 5 seconds)
              result match {
                case Some(p: Post) =>
                  if (PageService.updateFeeds(pageId, p)) {
                    complete("\"success\"")
                  }
                  else {
                    complete("failed")
                  }
              }
            }
          }
      }
    // no update or delete operation is supported by Facebook for this
  }
}
