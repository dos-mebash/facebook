package edu.ufl.facebook.routes

import edu.ufl.facebook.Utility
import edu.ufl.facebook.entity._


/**
  * Created by mebin on 11/30/15 for Facebook
  */
class PictureRouter extends Router {
  def receive = runRoute(pictureRouter)

  val pictureRouter =  {
    import spray.httpx.SprayJsonSupport.sprayJsonUnmarshaller
    import PictureJsonImplicits._
    getJson {
      path("all") {
        complete(PictureService.futureToJSONWait(PictureRepository.findAll))
      } ~
        path("allIds") {
          complete(PictureService.futureToJSONWait(PictureRepository.findAllIds))
        } ~
        path(Segment) {
          id =>
            val f = PictureRepository.findById(id)
            complete(PictureService.futureToJSONWait(f))
        }
    } ~
      put {
        path(Segment / "like") {
          userId =>
            path(Segment) {
              picId =>
                PictureService.updateLike(picId, userId)
                complete("success")
            }
        }
      } ~
      post {
        path("save") {
          entity(as[Picture]) { pic: Picture =>
            val pica = pic.copy(id = Utility.uuid)
            val f = PictureRepository.save(pica)
            complete(PictureService.futureToJSONWait(f))
          }
        }
      }
  }
}
