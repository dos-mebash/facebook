package edu.ufl.facebook


import java.security.{KeyPair}
import akka.actor.{Props, ActorSystem}
import akka.routing.RoundRobinPool
import edu.ufl.facebook.routes._
import edu.ufl.facebook.security.RSA
import spray.routing.SimpleRoutingApp


/**
  * Created by mebin on 11/8/15 for Facebook
  */
object Application extends App with SimpleRoutingApp {
  // config and default overrides, and then resolves it.
  implicit val actorSystem = ActorSystem("facebook")
  val userRouter = actorSystem.actorOf(Props(new UserRouter).withRouter(RoundRobinPool(5)))
  val profileRouter = actorSystem.actorOf(Props(new ProfileRouter).withRouter(RoundRobinPool(5)))
  val postRouter = actorSystem.actorOf(Props(new PostRouter).withRouter(RoundRobinPool(5)))
  val pictureRouter = actorSystem.actorOf(Props(new PictureRouter).withRouter(RoundRobinPool(5)))
  val pageRouter = actorSystem.actorOf(Props(new PageRouter).withRouter(RoundRobinPool(5)))
  val likeRouter = actorSystem.actorOf(Props(new LikesRouter).withRouter(RoundRobinPool(5)))
  val albumRouter = actorSystem.actorOf(Props(new AlbumRouter).withRouter(RoundRobinPool(5)))
  val circleRouter = actorSystem.actorOf(Props(new CircleRouter).withRouter(RoundRobinPool(5)))
  implicit val dispatcher = actorSystem.dispatcher

  val loadedKeyPair: KeyPair = RSA.loadKeyPair("C://Users//Ashish//Documents//", "RSA")

  startServer(interface = "localhost", port = 8080) {

    pathPrefix("user") { ctx => userRouter ! ctx } ~
      pathPrefix("profile") { ctx => profileRouter ! ctx } ~
      pathPrefix("post") { ctx => postRouter ! ctx } ~
      pathPrefix("picture") { ctx => pictureRouter ! ctx } ~
      pathPrefix("page") { ctx => pageRouter ! ctx } ~
      pathPrefix("like") { ctx => likeRouter ! ctx } ~
      pathPrefix("album") { ctx => albumRouter ! ctx } ~
      pathPrefix("circle") { ctx => circleRouter ! ctx }
  }
}