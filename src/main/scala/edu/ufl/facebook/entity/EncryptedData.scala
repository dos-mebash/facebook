package edu.ufl.facebook.entity


import spray.json.DefaultJsonProtocol
/**
  * Created by Ashish on 12/14/2015.
  */
case class EncryptedData(entityId:String, iv:String, encryptedEntity:String)

object EncryptedDataJsonImplicits extends DefaultJsonProtocol{
  implicit val encData = jsonFormat3(EncryptedData)
}
