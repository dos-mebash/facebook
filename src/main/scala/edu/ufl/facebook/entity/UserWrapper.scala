package edu.ufl.facebook.entity

import spray.json.DefaultJsonProtocol

/**
  * Created by Ashish on 12/12/2015.
  */
case class UserWrapper (
  user: User,
  digest: String
)

object UserWrapperJsonProtocol extends DefaultJsonProtocol {
  import UserJsonImplicits._
  implicit val UserWrapperId = jsonFormat2(UserWrapper)
}
