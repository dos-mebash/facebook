package edu.ufl.facebook.entity

import edu.ufl.facebook.Utility
import edu.ufl.facebook.entity.PostJsonImplicits
import spray.json.DefaultJsonProtocol

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by mebin on 11/19/15 for Facebook
  */

case class Page(id: String,
                name: String, //The name of the page
                ownerID: String, //Id of the Admin of the Page
                about: Option[String], // Info about the page
                //affiliation: Option[String], //Affiliation of this person. Applicable to pages representing people.
                artists_we_like: Option[List[String]], //Artists the band likes. Applicable to Bands
                //attire: Option[String], /*Dress code of the business. Applicable to Restaurants or Nightlife. Can be one of Casual, Dressy or Unspecified*/
                //awards: Option[String], //The award information of the film. Applicable to Films
                band_interests: Option[String], //Band interests. Applicable to Bands
                //albumList: List[String], // List of ids of the albums
                // Additional Features
                category: Option[String], //The Page's category. e.g. Product/Service, Computers/Technology
                pages_We_Like: Option[String], //List of pages like by Page
                //access_token: Option[String], //The access token you can use to act as the Page. Only visible to Page Admins
                //mailing_List: Option[String], //List of people to contact to
                //is_Published: Option[String], //Indicates whether the Page is published and visible to non-admins
                link: Option[String], //The Page's Facebook URL
                username: Option[String], //The alias of the Page. For example, for www.facebook.com/platform the username is 'platform'
                likes: Option[List[String]], //The number of users who likes this page

                // Edges
                albums: Option[String], //Photo albums for this Page
                //blocked: Option[String], //Users blocked from this Page
                events: Option[String], //Events for this Page
                photos: Option[List[Picture]], //This Pages Photos
                profilePic: Option[Picture], //This Page's profile picture. No access token is required to access this edge
                //roles: Option[String], //roles in Page's Admins
                feeds: Option[List[Post]], //	The feed of posts (including status updates) and links published by this page, or by others on this page.
                notes: Option[List[String]]//A Facebook Note created by a user or page.*/
                //key: String
               ) extends ID


object PageJsonImplicits extends DefaultJsonProtocol {

  import PictureJsonImplicits._
  import PostJsonImplicits._

  implicit val page = jsonFormat17(Page)
}

object PageService extends Service[Page] {

  /** @return User instance of the specified user
    * @param name of the user.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def getPageName(name: String): Page = {
    PageRepository.findByName(name)
  }

  /** @return User instance of the specified user
    * @param id id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def getPage(id: String): Page = {
    val f = PageRepository.findById(id)
    Await.result(f, 2 milliseconds) match {
      case Some(page: Page) => page
    }
  }

  /** @return friendlist of the user
    * @param id id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def getArtistList(id: String): Option[List[String]] = {
    var artists: Option[List[String]] = null
    val u = PageRepository.findById(id)
    var result = Await.result(u, 2 milliseconds) match {
      case Some(page: Page) => artists = page.artists_we_like
    }
    artists
  }


  /** @return friendRequests of the user
    * @param id id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def getLikes(id: String): Option[List[String]] = {
    val u = PageRepository.findById(id)
    Await.result(u, 2 seconds) match {
      case Some(page: Page) => page.likes
    }
  }

  def setLikes(pageId: String, userId: String) = {
    //    val p = Utility.waitForFuture(PageRepository.findById(pageId)).asInstanceOf[Option[Page]]
    val u = PageRepository.findById(pageId)
    Await.result(u, 2 seconds) match {
      case Some(page: Page) => page.likes match {
        case Some(p) =>
          val lik = userId :: p
          val pageCopy = page.copy(likes = Option(lik))
          PageRepository.update(pageCopy)
        case None =>
          val pageCopy = page.copy(likes = Option(List(userId)))
          PageRepository.update(pageCopy)
      }
    }
  }


  /** @return get list of photos in which this person is tagged
    * @param id id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def getPhotos(id: String): Option[List[Picture]] = {
    var photos: Option[List[Picture]] = null
    val u = UserRepository.findById(id)
    Await.result(u, 2 milliseconds) match {
      case Some(page: Page) => page.photos
    }

  }

  def updatePhotos(pageId: String, photo: Picture): Boolean = {
    val f = PageRepository.findById(pageId)
    Await.result(f, 2 milliseconds) match {
      case Some(page: Page) => {
        page.photos match {
          case Some(p) => {
            val alb = photo :: p
            val pageCopy = page.copy(photos = Option(alb.asInstanceOf[List[Picture]]))
            PageRepository.update(pageCopy)
          }
          case None =>
            val pageCopy = page.copy(photos = Option(List(photo)))
            PageRepository.update(pageCopy)
        }
        true
      }
      case None => false
    }
  }

  /** @return Boolean representing if update was successfull or not
    * @param pageId id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def updatePageName(pageId: String, pName: String): Boolean = {
    var uResult: Boolean = false
    val f = PageRepository.findById(pageId)
    var result = Await.result(f, 2 milliseconds) match {
      case Some(page: Page) => {
        val pageCopy = page.copy(name = pName)
        PageRepository.save(pageCopy)
        uResult = true
      }
      case None => uResult = false
    }
    uResult
  }

  def updateFeeds(pageId: String, feed: Post): Boolean = {
    var uResult: Boolean = false
    val f = PageRepository.findById(pageId)
    var result = Await.result(f, 2 seconds) match {
      case Some(page: Page) => {
        page.feeds match {
          case Some(p) => {
            val posts = feed :: p
            val pageCopy = page.copy(feeds = Option(posts))
            PageRepository.update(pageCopy)
          }
          case None => val pageCopy = page.copy(feeds = Option(List(feed)))
            PageRepository.update(pageCopy)
        }
        uResult = true
      }
      case None => uResult = false
    }
    uResult
  }

  /** @return Boolean representing if update was successfull or not
    * @param pageId id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def updateCategory(pageId: String, cName: String): Boolean = {
    var uResult: Boolean = false
    val f = PageRepository.findById(pageId)
    var result = Await.result(f, 2 milliseconds) match {
      case Some(page: Page) => {
        val pageCopy = page.copy(category = Some(cName))
        PageRepository.save(pageCopy)
        uResult = true
      }
      case None => false
    }
    uResult
  }

  /** @return Boolean representing if update was successfull or not
    * @param pageId id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def UpdateAbout(pageId: String, uAbout: String): Boolean = {
    var uResult: Boolean = false
    val f = PageRepository.findById(pageId)
    var result = Await.result(f, 2 milliseconds) match {
      case Some(page: Page) => {
        val pageCopy = page.copy(about = Some(uAbout))
        PageRepository.save(pageCopy)
        uResult = true
      }
      case None => false
    }
    uResult
  }

  //def deletePage(pageId: Int): Boolean = PageRepository.delete(pageId)
}

object PageRepository extends CRUDRepository[Page] {
  private var pageNameIDMap: Map[String, Page] = Map()

  def findByName(pageName: String): Page = {
    if (!pageNameIDMap.contains(pageName)) {
      map.values foreach (x => if (x.name == pageName) {
        pageNameIDMap += (x.name -> x)
      })
    }
    pageNameIDMap.get(pageName) match {
      case Some(user) => user
      case None => throw new Exception(s"Bhai kaun hain ye!!! ${pageName}")
    }
  }
}
