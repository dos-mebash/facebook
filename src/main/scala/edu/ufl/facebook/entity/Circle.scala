package edu.ufl.facebook.entity

import scala.collection.mutable
import scala.collection.mutable._
/**
  * Created by Ashish on 12/14/2015.
  */
case class Circle(id: String,
                  ownerId: String,
                  idAndEncKeyMap: Map[String, String]
                 )

object Circle {
  // Map of user id and list of circles
  val idAndListOfCircles: Map[String, ListBuffer[Circle]] = HashMap[String, ListBuffer[Circle]]()

  def addCircle(userId: String, circle: Circle): Unit = {
    val listOfCircles = idAndListOfCircles.getOrElse(userId, mutable.ListBuffer[Circle](circle))
    idAndListOfCircles += (userId -> listOfCircles)
  }
}