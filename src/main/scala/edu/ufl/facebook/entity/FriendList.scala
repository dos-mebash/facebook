//package edu.ufl.facebook.entity
//
//import spray.json.DefaultJsonProtocol
//import scala.concurrent.duration._
//import scala.concurrent.Await
//
///**
// * Created by mebin on 11/20/15 for Facebook
// * A friend list - an object which refers to a grouping of friends created by someone or generated automatically for someone (such as the "Close Friends" or "Acquaintances" lists). You can view all of a   * person's friend lists using the /{user-id}/friendlists edge.
// * Created by mebin on 11/20/15 for Facebook
// */
//case class FriendList(name: String, //The name of the friend list
//                      id: String, // The friend list id
//                      listType: Option[String], // The type of friend list
//                      owner: Profile, // The owner of the friend list
//                      members: Option[List[String]] // The profiles that are members of this lise
//                       ) extends ID
//
//object FriendListJsonImplicits extends DefaultJsonProtocol {
//  import ProfileJsonImplicits._
//  implicit val friendList = jsonFormat5(FriendList)
//}
//
//object FriendListService extends Service[FriendList]{
//
//  def getFriendListDetails(id:String): FriendList = {
//    var friendListDetails:FriendList = null
//    val u = FriendListRepository.findById(id)
//    var result = Await.result(u, 2 milliseconds)match {
//      case Some(friendList:FriendList) => friendListDetails = friendList
//    }
//    friendListDetails
//  }
//
//  def getOwner(id:String): Profile = {
//    getFriendListDetails(id).owner
//  }
//
//}
//
//object FriendListRepository extends CRUDRepository[FriendList]
