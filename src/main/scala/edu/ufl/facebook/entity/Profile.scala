package edu.ufl.facebook.entity

import spray.json.DefaultJsonProtocol
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by mebin on 11/20/15 for Facebook
 */
case class Profile(
                id: String,  // The Id of the profile
								name:String, //The name of the profile.
                linkUri: Option[String], // The link for this profile. Can be null.
								profile_Pic:Option[Picture] // The profile picture of the profile/user
                  ) extends  ID

object ProfileJsonImplicits extends DefaultJsonProtocol {
  import PictureJsonImplicits._
  implicit val profile = jsonFormat4(Profile)
}

object ProfileService extends Service[Profile]{

	def getProfileDetails(id: String): Profile = {
		var profileDetails: Profile = null
		val u = ProfileRepository.findById(id)
		var result = Await.result(u, 2 milliseconds)match {
			case Some(profile:Profile) => profileDetails = profile
		}
		profileDetails
	}

	def getProfileName(id:String): String = {
		getProfileDetails(id).name
	}

	def getProfilePic(id:String): Option[Picture] = {
		getProfileDetails(id).profile_Pic
	}

  def updateName(id:String, newName:String): Boolean = {
    var uResult:Boolean =  false
    val u = ProfileRepository.findById(id)
    var result = Await.result(u, 2 milliseconds)match {
      case Some(profile:Profile)=>
        val profileCopy = profile.copy(name =  newName)
        var f = ProfileRepository.update(profileCopy)
        uResult = true
    }
    uResult
  }

  def updateProfilePic(id: String, newPic:Option[Picture]): Boolean = {
    var uResult:Boolean =  false
    val u = ProfileRepository.findById(id)
    var result = Await.result(u, 2 milliseconds)match {
      case Some(profile:Profile)=>
        val profileCopy = profile.copy(profile_Pic = newPic)
        var f = ProfileRepository.update(profileCopy)
        uResult = true
    }
    uResult
  }
}

object ProfileRepository extends CRUDRepository[Profile]
