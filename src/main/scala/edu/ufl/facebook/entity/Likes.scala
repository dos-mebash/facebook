package edu.ufl.facebook.entity

import edu.ufl.facebook.entity.PostRepository._
import spray.json.DefaultJsonProtocol
import scala.concurrent.duration._
import scala.collection.mutable
import scala.concurrent.Await

/**
 * Created by mebin on 11/25/15 for Facebook
 */
case class Likes (
                 id:String,
                 totalCount:Int // total count of likes on the object
                 //users:mutable.MutableList // list of users who like it
                 )extends ID

object LikesJsonImplicits extends DefaultJsonProtocol {
  implicit val impLikes = jsonFormat2(Likes)
}

object LikesService extends Service[Likes]{

  def getTotalLikes(id:String):Int = {
    var tCount:Int = -1
    var u = LikesRepository.findById(id)
    var result = Await.result(u, 2 milliseconds)match{
      case Some(like:Likes) => tCount = like.totalCount
    }
    tCount
  }
  def setTotalLikes(id:String):Boolean = {
    var bResult:Boolean=false
    val u = LikesRepository.findById(id)
    var result = Await.result(u, 2 milliseconds)match {
      case Some(like:Likes) =>
          val likeCopy = like.copy(totalCount = like.totalCount+1)
          val f = LikesRepository.update(likeCopy)
          bResult = true
    }
    bResult
  }

}

object LikesRepository extends CRUDRepository[Likes]