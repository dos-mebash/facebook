package edu.ufl.facebook.entity

import scala.concurrent.Future

/**
 * Created by mebin on 11/19/15 for Facebook
 */
trait CRUDRepository[T <: ID] {

  import edu.ufl.facebook.Application._

  var map: Map[String, T] = Map[String, T]()

  def findById(id: String): Future[Option[T]] = {
    Future {
      map.get(id)
    }
  }

  def findAll: Future[Option[List[T]]] = {
    Future{
      Option(map.values.toList)
    }
  }

  def findAllIds : Future[Option[List[String]]] = {
    Future{
      Option(map.keys.toList)
    }
  }

  def update(u:T): Future[Option[String]] = {
    Future{
      map += (u.id -> u)
      Some("success")
    }
  }

  def save(u: T): Future[Option[String]] = {
    Future {
      if (map.contains(u.id)){
        Some(s"Already exists id : ${u.id}")
      }
      else{
        map += (u.id -> u)
        Some("success")
      }
    }
  }

  def delete(id: String): Future[Option[String]] = {
    Future {
      if (map contains id) {
        map -= id
        Some(true.toString)
      }
      else {
        Some(false.toString)
      }
    }
  }
}