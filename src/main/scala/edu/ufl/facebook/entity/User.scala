package edu.ufl.facebook.entity

import java.security.PublicKey
import edu.ufl.facebook.security.RSA
import spray.json.DefaultJsonProtocol

import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Created by mebin on 11/8/15 for Facebook
 */

case class User(name: String,
                id: String,
                userName: String,
                password: String,
                firstName: Option[String],
                lastName: Option[String],
                about: Option[String],
                friendLists: Option[List[String]],
                bio: Option[String],
                birthday: Option[String],
                email: Option[String],
                gender: Option[String],
                friendRequests: Option[List[String]],
                //                      accounts: Option[List[Page]], // Facebook Pages this person administers/is an admin for
                photos: Option[List[EncryptedData]], // The list of Picture of the user
                posts: Option[List[EncryptedData]],
                key: String
                 ) extends ID

object UserJsonImplicits extends DefaultJsonProtocol {

  import PictureJsonImplicits._
  import PostJsonImplicits._
  import EncryptedDataJsonImplicits._

  implicit val impPerson = jsonFormat16 (User)
}

object UserService extends Service[User] {


  /** @return User instance of the specified user
    * @param name of the user.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def getUserName(name: String): User = {
    UserRepository.findByName(name)
  }

  /** @return User instance of the specified user
    * @param id id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def getUser(id: String): User = {
    var u: User = null
    val f = UserRepository.findById(id)
    var result = Await.result(f, 2 seconds) match {
      case Some(user: User) => u = user
      case None => println(s"Error user id ${id} was requested")
        null
    }
    u
  }

  /** @return friendlist of the user
    * @param id id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def getFriendList(id: String): Option[List[String]] = {
    val u = UserRepository.findById(id)
    Await.result(u, 2 seconds) match {
      case Some(user: User) => user.friendLists
    }
  }


  def addFriend(userId: String, friendId: String, friendEncKey: String): Boolean = {
    var uResult: Boolean = false
    //Add to friends circle
    val listOfCirclesOfUser = Circle.idAndListOfCircles(userId)
    val friendListCircle = listOfCirclesOfUser(0) // assumed to be the first one
    friendListCircle.idAndEncKeyMap += (friendId -> friendEncKey)
    val f = UserRepository.findById(userId)
    Await.result(f, 2 seconds) match {
      case Some(user: User) => user.friendRequests match {
        case Some(p) => if (p.contains(friendId)) {
          user.friendLists match {
            case Some(friendList) =>
              val frnd = friendId :: friendList
              var userCopy = user.copy(friendLists = Option(frnd))
              val frndRqst = p.filter(_ != friendId)
              userCopy = userCopy.copy(friendRequests = Option(frndRqst))
              UserRepository.update(userCopy)
            case None =>
              var userCopy = user.copy(friendLists = Option(List(friendId)))
              val frndRqst = p.filter(_ != friendId)
              userCopy = userCopy.copy(friendRequests = Option(frndRqst))
              UserRepository.update(userCopy)

          }
          uResult = true
        }
        else {
          uResult = false
        }
      }
    }
    uResult
  }

  /** @return friendRequests of the user
    * @param id id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015

  def getFriendRequest(id: Int): List[User] = {
    val u = UserRepository.findById(id)
    val user: User = u match {
      case Some(user) => user
    }
    user.friendRequests
  }
    */


  /** @return get list of friend requests this User has received
    * @param id id of the user requested.
    * @author  Ashish Nautiyal
    * @version 1.0, 24/11/2015
    */
  def getFriendRequests(id: String): String = {
    val u = UserRepository.findById(id)
    val friendRequests = Await.result(u, 2 seconds) match {
      case Some(user: User) => user.friendRequests
    }
    friendRequests match {
      case Some(friendRequest) => toJSON(friendRequest)
      case None => ""
    }
  }

  /** @return get list of photos in which this person is tagged
    * @param id id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def getPhotos(id: String): Option[List[Picture]] = {
    var photosList: Option[List[Picture]] = null
    val u = UserRepository.findById(id)
    var result = Await.result(u, 2 seconds) match {
      case Some(user: User) => photosList //= user.photos
    }
    photosList
  }

  /** @return Boolean representing if update was successfull or not
    * @param userId id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def updateName(userId: String, fName: String): Boolean = {
    ProfileService.updateName(userId, fName)
    var uResult: Boolean = false
    val u = UserRepository.findById(userId)
    var result = Await.result(u, 2 seconds) match {
      case Some(user: User) => {
        val userCopy = user.copy(name = fName)
        val u = UserRepository.update(userCopy)
        uResult = true
      }
    }
    uResult
  }

  /** @return Boolean representing if update was successfull or not
    * @param userId id of the user requested.
    * @author  Mebin Jacob
    * @version 1.0, 24/11/2015
    */
  def updateFriendRequests(userId: String, friendId: String, friendEncKey: String): Boolean = {
    var uResult: Boolean = false
    val f = UserRepository.findById(friendId)
    //Add to friends circle
    val listOfCirclesOfUser = Circle.idAndListOfCircles(userId)
    val friendListCircle = listOfCirclesOfUser(0) // assumed to be the first one
    friendListCircle.idAndEncKeyMap += (friendId -> friendEncKey)
    Await.result(f, 2 seconds) match {
      case Some(user: User) => user.friendRequests match {
        case Some(p) => {
          val frndReqsts = userId :: p
          val userCopy = user.copy(friendRequests = Option(frndReqsts))
          UserRepository.update(userCopy)
        }
        case None => val userCopy = user.copy(friendRequests = Option(List(userId)))
          UserRepository.update(userCopy)
          uResult = true
      }
      case None => uResult = false
    }
    uResult
  }


  def friendRequestAccepted(userId: String, friendId: String): Boolean = {
    var uResult: Boolean = false
    val f = UserRepository.findById(userId)
    Await.result(f, 2 seconds) match {
      case Some(user: User) => user.friendLists match {
        case Some(p) => {
          val frndLists = friendId :: p
          val userCopy = user.copy(friendLists = Option(frndLists))
          UserRepository.update(userCopy)
        }
        case None => val userCopy = user.copy(friendLists = Option(List(friendId)))
          UserRepository.update(userCopy)
          uResult = true
      }
      case None => uResult = false
    }
    uResult
  }

  def updateFeeds(pageId: String, feed: EncryptedData): Boolean = {
    var uResult: Boolean = false
    val f = UserRepository.findById(pageId)
    var result = Await.result(f, 2 seconds) match {
      case Some(user: User) => {
        user.posts match {
          case Some(p) => {
            val posts = feed :: p
            val pageCopy = user.copy(posts = Option(posts))
            UserRepository.update(pageCopy)
          }
          case None => val pageCopy = user.copy(posts = Option(List(feed)))
            UserRepository.update(pageCopy)
        }
        uResult = true
      }
      case None => uResult = false
    }
    uResult
  }
}

object UserRepository extends CRUDRepository[User] {
  private var userNameIDMap: Map[String, User] = Map()

  def findByName(userName: String): User = {
    if (!userNameIDMap.contains(userName)) {
      map.values foreach (x => if (x.name == userName) {
        userNameIDMap += (x.name -> x)
      })
    }
    userNameIDMap.get(userName) match {
      case Some(user) => user
      case None => throw new Exception(s"Bhai kaun hain ye!!! ${userName}")
    }
  }
}