package edu.ufl.facebook.entity

import akka.actor.ActorSystem
import org.json4s.NoTypeHints
import org.json4s.native.Serialization
import org.json4s.native.Serialization._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._


/**
 * Created by mebin on 11/20/15 for Facebook
 */
trait Service[T <: AnyRef] {
  implicit val actorSystem = ActorSystem("facebook")
  implicit val dispatcher = actorSystem.dispatcher
  private implicit val formats = Serialization.formats(NoTypeHints)

  def toJSON[T <: AnyRef](o: T): String = writePretty(o)

  def futureAndDurationToJSON[T <: AnyRef](f: Future[Option[T]], d: Duration): String = {
    Await.result(f, d) match {
      case Some(user) => this.toJSON(user)
      case None => "conversion failed"
    }
  }

  def futureToJSONWait[T <: AnyRef](f: Future[Option[T]]): String = {
    futureAndDurationToJSON(f, 10 seconds)
  }

  def futureToJSON[T <: AnyRef](f: Future[Option[T]]): Unit = {
    f onFailure{
      case _ => println("Got screwed while running the future in Service.scala")
    }
  }
}
